[![pipeline status](https://gitlab.com/Luke594/Algoweb/badges/master/pipeline.svg)](https://gitlab.com/Luke594/Algoweb/commits/master)

[PDF preview](https://gitlab.com/Luke594/Algoweb/-/jobs/artifacts/master/file/book.pdf?job=compile_pdf)

[Download PDF](https://gitlab.com/Luke594/Algoweb/-/jobs/artifacts/master/raw/book.pdf?job=compile_pdf)


# Idea
Obiettivo del lavoro è costruire una dispensa autocontenuta riguardo le principali tematiche trattate in ambito Web.

Stato di avanzamento del lavoro → [wiki](https://gitlab.com/Luke594/Algoweb/wikis/home)

# Linee di massima
Tipicamente, quando si lavora ad un progetto a più mani (adottando un opportuno tool di *versioning* quale **git**), capita che si verifichino diverse tipologie di scenari: ad esempio, può capitare di procedere in maniera sincronizzata, o desincronizzata, sulle medesime linee di codice, oppure su linee di codice completamente differenti (o addirittura su files differenti), e così via...

Risultato: la probabilità che si verifichino conflitti è alta!

Fintanto che il sistema riesce a risolvere automaticamente i conflitti, non c'è alcun problema. In caso contrario, invece, si può procedere in due modi:

- in maniera errata, mediante il comando ```$ git pull origin master```
- in maniera corretta, mediante il comando ```$ git fetch origin master; git rebase origin/master```

Una volta intrapresa la via corretta, occorre modificare il sorgente, selezionando quali modifiche in conflitto occorre portare avanti. Tali modifiche sono evidenziate in questa maniera:
```
<<<<<<< HEAD
...
=======
...
>>>>>>> Commit message
```
**Nota**: se per errore si dovesse intraprendere la prima via, reperire con ```$ git log``` l'hash del commit appena generato; dopodiché inserire il comando ```$ git reset --hard hash```, da cui è possibile intraprendere la via corretta.
