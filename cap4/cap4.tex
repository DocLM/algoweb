\chapter{Risoluzione delle interrogazioni}
\label{ch:query-resoluzion}
Il processo di risoluzione di interrogazioni nasce da una necessità informativa da parte dell'utente.
Tipicamente, la risoluzione comincia somministrando un insieme di parole chiave ad un motore di ricerca, le quali però, prima di poter essere gestite, devono essere elaborate mediante dei criteri di normalizzazione analoghi a quelli che sono stati applicati ai termini estratti dai documenti. Il risultato di tale processo prende il nome di \textit{query}.

%-------------------------------------------------------------------------------------%
\section{Motori di ricerca booleani}
\label{sec:boolean-search-engines}
Un motore di ricerca booleano risponde alle interrogazioni trasformando le parole chiave in formule logiche. Il risultato della risoluzione delle \textit{query} è dato dall'insieme di tutti i documenti che verificano il predicato logico.

%-------------------------------------------------------------------------------------%
\subsection{Sintassi e semantica delle query}
\label{subsec:query-syntax-semantics}
Le \textit{query} di un motore booleano devono rispettare una precisa sintassi. In particolare, il termine base è una proposizione atomica $t$, a cui possono essere applicate operazioni logiche come l'operatore di negazione ($\lnot$), l'operatore di disgiunzione ($\lor$) e l'operatore di congiunzione ($\land$). 
D'altra parte, ogni \textit{query} deve essere anche contestualizzata rispetto ai dati presenti nell'indice del motore, associando una determinata semantica: infatti, ogni occorrenza di un certo termine $t$ viene sostituita con la relativa lista di affissioni $\llbracket t \rrbracket$, contenente l'insieme dei documenti in cui $t$ compare. 

\greybox{Osservazione}{%
Da un punto di vista insiemistico, $\llbracket t \rrbracket$ è un insieme $S \subseteq D$, essendo $D$ l'insieme di tutti i documenti presenti nella collezione.
}

\noindent Analizzando gli operatori logici applicati alle \textit{query}, è possibile notare che vengono tradotti in operazioni su insiemi nel seguente modo:
\begin{itemize}
	\item $ \llbracket \lnot q \rrbracket = D \setminus \llbracket q \rrbracket$, cioè l'intero insieme di documenti esclusi quelli che soddisfano $q$;
	\item $\llbracket q_1 \lor q_2 \rrbracket = \llbracket q_1 \rrbracket \cup \llbracket q_2 \rrbracket$, cioè l'unione delle liste di affissioni che soddisfano $q_1$ e $q_2$;
	\item $\llbracket q_1 \land q_2 \rrbracket = \llbracket q_1 \rrbracket \cap \llbracket q_2 \rrbracket$, cioè l'intersezione delle liste di affissioni che soddisfano $q_1$ e $q_2$.
\end{itemize}

%-------------------------------------------------------------------------------------%
\subsection{Risoluzione tramite iteratori}
\label{subsec:iterators}
Un motore di ricerca che sia in grado di risolvere \textit{query} per una grande quantità di utenti deve necessariamente avere un'architettura scalabile. 
Dal momento che è impensabile caricare completamente in memoria le liste di affissioni di tutti i termini di una \textit{query}, una strategia plausibile consiste nell'adottare un approccio \textit{lazy}, facendo leva su una serie di iteratori\footnote{Un iteratore è un oggetto che incapsula lo stato di una determinata iterazione.}.

\begin{itemize}
	\item \textbf{Iteratore per le liste di affissioni}: permette di ottenere la lista di affissioni associata al termine $t$. Un algoritmo che può essere utilizzato è il seguente:
	\begin{enumerate}
		\item si normalizza il termine $t$ a seconda delle politiche del motore;
		\item si accede al file dell'indice contenente la lista di affissioni di $t$;
		\item si costruisce un iteratore, effettuando una lettura in \textit{streaming} dei dati dal file solo se richiesti.
	\end{enumerate}
\end{itemize}

\vspace*{-.5\baselineskip}
\greybox{Osservazione}{%
Per favorire la risoluzione booleana, è possibile pensare di ristrutturare l'indice in più file, ad esempio dividendo in tre file distinti le informazioni relative a documenti, conteggi e posizioni. In questo modo, la dimensione dei file contenenti le liste di affissioni risulta ridotta, permettendo una lettura e una risoluzione delle \textit{query} più veloci.
Chiaramente, questo approccio porta con sé alcuni effetti collaterali, applicabili a tutti quei motori di ricerca più elaborati che risolvono \textit{query} accedendo a più file in contemporanea: infatti, disponendo dei classici dischi HDD (\textit{Hard Disk Drive}), l'accesso ad un file deve necessariamente avvenire in maniera sequenziale.
Tuttavia, con l'avvento di nuove tecnologie di memorizzazione, come per esempio i dischi SSD (\textit{Solid-State Drive}), il costo di ogni lettura è stato notevolmente ammortizzato.
}

\begin{itemize}
	\item \textbf{Iteratore per la negazione di formule}: permette di risolvere \textit{query} nella forma $\llbracket \lnot q \rrbracket$. Un meccanismo che si può applicare è il seguente:
	\begin{enumerate}
		\item si costruisce un iteratore associato a $q$;
		\item si costruisce un iteratore che scorre la collezione di tutti documenti $D$;
		\item si memorizza in una variabile $a$ il valore di $q$ attuale;  
		\item facendo avanzare l'iteratore associato a $D$, si restituiscono tutti i documenti, fino ad $a$ escluso;
		\item si fa avanzare l'iteratore di $q$ e si ripete dal punto $3$.
	\end{enumerate}
	\item \textbf{Iteratore per la disgiunzione di formule}: permette di risolvere \textit{query} nella forma $\llbracket q_1 \lor q_2 \rrbracket$. Una procedura che può essere sfruttata consiste in quanto segue:
	\begin{enumerate}
		\item si costruiscono gli iteratori associati a $q_1$ e $q_2$;
		\item si applica un algoritmo di fusione di liste per unire i risultati degli iteratori di $q_1$ e $q_2$.
	\end{enumerate}
\end{itemize}

\vspace*{-.5\baselineskip}
\greybox{Osservazione}{%
Per risolvere \textit{query} composte da più termini, nella forma $\llbracket q_1 \lor q_2 \lor \ldots \lor q_n \rrbracket$, si applica un algoritmo di \textit{fusione multi-way}, trattato in corrispondenza della \autoref{subsec:multi-way-fusion}.
In particolare, è evidente che, trattandosi di unione di liste, non ci si può esimere dal leggere tutte le liste di affissioni associate a ogni termine.
}

\begin{itemize}
	\item \textbf{Iteratore per la congiunzione di formule}: permette di risolvere \textit{query} nella forma $\llbracket q_1 \land q_2 \rrbracket$. Una strategia che si può applicare è riportata nel seguito:
	\begin{enumerate}
		\item si costruiscono gli iteratori associati a $q_1$ e $q_2$;
		\item per poter tener traccia del documento con identificatore massimo tra tutti i documenti correnti, si utilizza una variabile $max$;
		\item per poter tener traccia del documento con identificatore minimo tra tutti i documenti correnti, si utilizza una coda con priorità indiretta;
		\item tenendo conto degli elementi puntati correntemente in ciascuna lista di affissioni, si prende in considerazione la lista in cui il valore corrente $i$ è il minimo e non supera $max$, quindi si fa avanzare il relativo iteratore;
		\item se $i=max$ in ogni lista di affissioni, $max$ viene restituito come risultato e si fanno avanzare tutti gli iteratori, ricalcolando i valori di massimo e minimo, altrimenti, se $i>max$, $i$ viene assegnato a $max$;
		\item l'algoritmo si ripete dal punto $4$ fino a quando un iteratore raggiunge la fine della propria lista di affissioni.
	\end{enumerate}
	\item[] In corrispondenza della \autoref{fig:Congiunzione}, è possibile trovare un esempio di esecuzione dell'algoritmo riportato poc'anzi.
\end{itemize}

\vspace*{-.5\baselineskip}
\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.31]{cap4/img/congiunzione}
	\caption{Esempio di risoluzione di una \textit{query} nella forma $\llbracket q_1 \land q_2 \land q_3 \rrbracket$.}
	\label{fig:Congiunzione}
\end{figure}

\begin{itemize}
	\item[] In realtà, questa procedura può essere migliorata aggiungendo sugli iteratori una primitiva, riportata nella \autoref{sec:jump-list}, per effettuare dei salti all'interno delle liste di affissioni: nel dettaglio, quando l'identificatore del documento corrente è il minimo e non supera $max$, si effettua un salto al primo documento con identificatore $i \geq max$. Per un esempio pratico di quanto detto, si faccia riferimento alla \autoref{fig:Congiunzione-ottimizzata}.
\end{itemize}

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.31]{cap4/img/congiunzione-ottimizzata}
	\caption{Miglioramento della risoluzione di una \textit{query} nella forma $\llbracket q_1 \land q_2 \land q_3 \rrbracket$.}
	\label{fig:Congiunzione-ottimizzata}
\end{figure}

\vspace*{-.5\baselineskip}
\greybox{Osservazione}{%
Se tra il valore minimo corrente ed il valore massimo esiste una differenza consistente, il salto permette di risparmiare la scansione di una porzione rilevante di documenti.
}

\begin{itemize}
	\item[] Una valida alternativa che sfrutta la tecnica di salto su $k$ liste di affissioni è data dal seguente algoritmo \textit{greedy}:
	\begin{enumerate}
		\item si allineano tra loro le prime due liste di affissioni rispetto al valore massimo $max$;
		\item date $n$ liste allineate tra loro rispetto a $max$, si allinea la lista $n+1$ a $max$, effettuando un salto al primo documento con identificatore $i \geq max$;
		\item se $i > max$, $i$ viene assegnato a $max$ e si riparte dal punto $1$.
	\end{enumerate}
\end{itemize}

\vspace*{-.5\baselineskip}
\greybox{Osservazione}{%
Qualsiasi informazione aggiuntiva connessa ai termini della collezione può essere chiaramente sfruttata per ottimizzare la risoluzione
\makebox[\textwidth][s]{delle \textit{query}. Ad esempio, essendo nota la frequenza di apparizione}%
}
%
\customgreybox{%
di un termine $t$, è possibile stabilire un ordine di allineamento tra le liste di affissioni. In particolare, dando priorità ai termini con minore frequenza, è possibile ridurre il numero di candidati con cui allineare gli iteratori associati alle altre liste\footnotemark.
}
\footnotetext{Per esempio, qualora si volesse cercare la stringa ``Romeo e Giulietta'', converrebbe prima allineare le liste di affissioni di ``Romeo'' e ``Giulietta'' (sparse), poi la lista di affissioni di ``e'' (densa).}

\noindent Quindi, prendendo in considerazione un ipotetico caso reale in cui un utente fornisca al motore di ricerca la \textit{query} ``affitto casa'', si verificherebbe una espansione semantica di ciascuna \textit{keyword}, come riportato in \autoref{fig:Gerarchia}.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.335]{cap4/img/gerarchia}
	\caption{Rappresentazione di una gerarchia di iteratori per risolvere la \textit{query} ``affitto casa''.}
	\label{fig:Gerarchia}
\end{figure}

\noindent Le gerarchie generate possono risultare enormemente complicate, arrivando a contenere anche centinaia di iteratori.
Il punto chiave è che la lista dei documenti risultanti viene sempre prodotta in maniera \textit{lazy}: infatti, l'I/O che si effettua è proporzionale solo a quanto si legge effettivamente.

%-------------------------------------------------------------------------------------%
\subsection{Early termination}
\label{subsec:early-termination}
Fondamentale è il concetto di \textit{laziness}, che nei moderni motori di ricerca si riflette nel fenomeno di \textit{early termination}: in particolare, quando occorre presentare un insieme di risultati all'utente, si effettua una lettura dell'indice di un certo numero di documenti, raggiunto il quale ci si interrompe.
Perciò, in termini pratici, la risoluzione di un'interrogazione non prevede la consultazione di tutti i documenti, bensì di una piccola frazione, che si spera sia sufficientemente rappresentativa per ciò che l'utente chiede.
A fronte di ciò, risulterebbe particolarmente utile disporre di un meccanismo per presentare i documenti in ordine di qualità decrescente. Tuttavia, l'adozione di una tecnica di questo tipo ha contribuito alla nascita di un fenomeno particolare, denominato ``Google Hell'': tale fenomeno affligge tutti i documenti che, nonostante risultino indicizzati, sono situati sul fondo delle liste di affissioni, e perciò presentati all'utente con scarsa probabilità, a meno di effettuare \textit{query} estremamente selettive.
Per fronteggiare questa problematica, sono emerse diverse strategie di \textit{Search Engine Optimization} (SEO) per cercare di aumentare la rilevanza di particolari gruppi di documenti durante le ricerche.
Dunque, in buona sostanza, la risoluzione di \textit{query} booleane nei moderni motori di ricerca è un passo fondamentale, in quanto viene utilizzata per costruire una base di documenti candidati da cui partire per applicare un punteggio ad ogni documento per selezionare quelli più rilevanti per l'utente.

%-------------------------------------------------------------------------------------%
\section{Risoluzione frasale}
\label{sec:phrasal-queries}
La risoluzione di \textit{query} frasali consiste nella ricerca di tutti i documenti in cui l'ordine di apparizione di una precisa sequenza di \textit{token} è rispettato.
Per esempio, si supponga di voler trovare tutti i documenti che contengono ``Romeo e Giulietta''.
Innanzitutto, occorre risolvere la \textit{query} booleana per individuare i documenti che contengono tutte le parole ricercate.
Una volta trovata una corrispondenza, si accede all'indice per ricavare le posizioni in cui ogni \textit{token} compare, consultando la relativa lista di affissioni. 
In questo caso, si assuma di aver ottenuto il seguente scenario:

\begin{table}[!h]
\centering
	\resizebox{.7\textwidth}{!}{
	\begin{tabular}{llllllll}
	Romeo		& $p_0$, & $p_1$, & $p_2$, & $p_3$, & $p_4$, & $p_5$, & \dots \\
	e			& $q_0$, & $q_1$, & $q_2$, & $q_3$, & $q_4$, & $q_5$, & \dots \\
	Giulietta	& $r_0$, & $r_1$, & $r_2$, & $r_3$, & $r_4$, & $r_5$, & \dots \\
	\end{tabular}}
\end{table}

\noindent L'obiettivo è quello di individuare i documenti in cui i vincoli $r_i = q_i + 1$ e $q_i = p_i + 1$ siano soddisfatti, per una qualsiasi $i \in \mathbb{N}$.
Quindi, per verificare la corrispondenza il più rapidamente possibile, si effettua una rinumerazione dei valori delle posizioni associate a ogni \textit{token}, in maniera conforme ai vincoli stabiliti, ottenendo:

\begin{table}[!h]
\centering
	\resizebox{\textwidth}{!}{
	\begin{tabular}{llllllll}
	Romeo		& $p_0$, & $p_1$, & $p_2$, & $p_3$, & $p_4$, & $p_5$, & \dots \\
	e			& $q_0-1$, & $q_1-1$, & $q_2-1$, & $q_3-1$, & $q_4-1$, & $q_5-1$, & \dots \\
	Giulietta	& $r_0-2$, & $r_1-2$, & $r_2-2$, & $r_3-2$, & $r_4-2$, & $r_5-2$, & \dots \\
	\end{tabular}}
\end{table}

\noindent Quindi si applica una versione analoga dell'algoritmo analizzato nella \autoref{subsec:iterators} relativo alla intersezione di \textit{query}, restituendo tutti i documenti per cui si ottiene una corrispondenza.

\greybox{Osservazione}{%
\`E difficile che un utente riesca a prevedere precisamente quali tecniche (come per esempio la normalizzazione, lo \textit{stemming}, introdotto nel \autoref{ch:indexing}, la rimozione dei caratteri diacritici\footnotemark, l'espansione semantica, e così via) siano applicate dal motore di ricerca per trasformare ogni \textit{keyword} delle \textit{query} che ha formulato.
D'altro canto, un motore di ricerca non può aspettarsi in anticipo per quanto sia necessario scorrere le liste di affissioni di ciascuna \textit{keyword}: infatti, qualora si sottoponesse una \textit{query} frasale, costituita esclusivamente da \textit{stopwords} (ad esempio ``and then that of a''), potrebbero occorrere addirittura secondi prima di riuscire a visualizzare un risultato, anche vuoto.
}

\footnotetext{Un carattere diacritico (o solo diacritico) è un segno aggiunto a una lettera per modificarne la pronuncia o per distinguere il significato di parole simili: ad esempio \`{a}, \'{a}, \^{a}, \"{a}, \c{s}, e così via.}

%-------------------------------------------------------------------------------------%
\section{Segmentazione dell'indice}
\label{sec:index-splitting}
La segmentazione dell'indice è un processo necessario e fondamentale, che si basa sulla suddivisione dell'indice in più parti per permettere un accesso parallelo e garantire un miglioramento delle prestazioni nella risoluzione delle interrogazioni. In generale, esistono diversi criteri di suddivisione dell'indice.

%-------------------------------------------------------------------------------------%
\subsection{Segmentazione lessicale}
\label{subsec:lexical-splitting}
La strategia di segmentazione lessicale consiste nella suddivisione dell'indice isolando una determinata caratteristica assunta dai \textit{token} che compongono l'indice: per esempio, uno dei criteri più banali consiste nell'utilizzo dell'ordine lessicografico dei \textit{token}, riportato graficamente in corrispondenza della \autoref{fig:Server1}.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.47]{cap4/img/server1}
	\caption{Schematizzazione di segmentazione lessicale distribuita.}
	\label{fig:Server1}
\end{figure}

\noindent Analizzando più nel dettaglio questo sistema, è possibile notare che sono presenti due macchine distinte, ognuna pensata per gestire un determinato range di termini. Tali macchine sono collegate ad un server di risoluzione delle \textit{query} mediante la rete (solitamente dedicata e molto veloce).
Quando il server di risoluzione riceve una \textit{query}, risolve localmente la corrispondente \textit{query} booleana, richiedendo poi in maniera \textit{lazy} i \textit{token} alla macchina opportuna.
Chiaramente, adottare una strategia di questo tipo gode del vantaggio di distribuire il carico di accesso all'indice su più macchine, tuttavia soffre del fatto di essere lento, dal momento che tutti i dati non vengono più trasferiti dalla memoria centrale, ma passano attraverso la rete.

\greybox{Osservazione}{%
La segmentazione lessicale dell'indice risulta estremamente efficace se applicata all'interno di una singola macchina per distribuire il carico tra i vari supporti di memorizzazione, quali RAM, SSD e HDD: in particolare, sulla base di informazioni raccolte durante il funzionamento del motore di ricerca, come la frequenza con cui vengono richiesti alcuni termini, è possibile suddividere l'indice allocando in RAM i termini più frequenti, costruendo su SSD una coda per gestire la rimanenza dei termini e memorizzando su HDD i termini che vengono consultati molto raramente.
Infatti, si ricordi che in generale le \textit{query} assumono una distribuzione molto sghemba, perciò solo un migliaio di termini sono responsabili del $99\%$ delle interrogazioni.
}
\vspace*{.1\baselineskip}

%-------------------------------------------------------------------------------------%
\subsection{Segmentazione documentale}
\label{subsec:documents-splitting}
La strategia di segmentazione documentale prevede la memorizzazione di diverse porzioni delle liste di affissioni dei \textit{token} su macchine differenti. In particolare, ogni macchina contiene un certo numero $k$ di elementi appartenenti a ogni lista di affissioni dei \textit{token}. Una rappresentazione del sistema è riportata in corrispondenza della \autoref{fig:Server2}.
%
\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.47]{cap4/img/server2}
	\caption{Schematizzazione di segmentazione documentale distribuita.}
	\label{fig:Server2}
\end{figure}
%
Nel dettaglio, sono date due macchine distinte, ognuna progettata per occuparsi di un determinato range di documenti, le quali sono collegate ad un server di risoluzione delle \textit{query} mediante la rete (che, come analizzato nella \autoref{subsec:lexical-splitting}, è dedicata e molto veloce).
Durante la risoluzione della \textit{query}, il server combina i risultati, prelevando i documenti dalla macchina opportuna. Un approccio di questo tipo porta i seguenti vantaggi:
\begin{itemize}
	\item scalabilità dell'architettura, che consiste nel fatto di poter aggiungere al sistema nuove macchine in maniera del tutto trasparente, a seconda della quantità di dati in arrivo;
	\item parallelizzazione, in quanto è possibile risolvere direttamente le \textit{query} nelle macchine contenenti i documenti. Il server che si occupa della risoluzione si limita ad eseguire una concatenazione dei risultati ottenuti.
\end{itemize}
L'evidente svantaggio è che, per cercare un termine, occorre consultare tutte le macchine che si hanno a disposizione.

\greybox{Osservazione}{%
Nel caso in cui i documenti dovessero essere distribuiti tra le macchine in base al loro livello di ``rilevanza'', si potrebbe sfruttare l'\textit{early termination} per risolvere le \textit{query}, consultando esclusivamente le macchine contenenti i documenti più rilevanti.
}

%-------------------------------------------------------------------------------------%
\subsection{Conclusioni}
\label{subsec:index-splitting-end}
Una strategia interessante potrebbe essere quella di applicare i due approcci appena presentati in contemporanea, ad esempio applicando una segmentazione documentale su più macchine e implementando su ognuna una segmentazione lessicale.
In aggiunta, si potrebbe addirittura pensare di suddividere i dati tra le varie macchine tenendo conto anche della frequenza di aggiornamento: infatti, per esempio, potrebbe risultare utile disporre di una macchina che raccoglie i \textit{token} che vengono aggiornati giornalmente e di una macchina dedicata ai \textit{token} che vengono aggiornati meno frequentemente.
