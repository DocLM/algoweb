\chapter{Indexing}
\label{ch:indexing}
Nella anatomia di un motore di ricerca, l'\textit{indicizzazione} è una fase che si verifica subito dopo l'attività di crawling, in cui tutte le pagine che sono state faticosamente scaricate vengono trasformate in oggetti utili che contribuiscano a rispondere rapidamente alle interrogazioni poste dagli utenti.
Più precisamente, ciò che si ottiene in output è analogo all'indice di un libro: una lista di parole a cui è associata la relativa pagina da cui sono state estratte.

\greybox{Osservazione}{%
Per poter essere indicizzata, ogni pagina che è stata scaricata deve prima essere trasformata in una lista di \textit{token}, ovvero dei segmenti di testo identificati in maniera univoca.
Solitamente, di una pagina può valere la pena indicizzare anche i \textit{tag}, come accade per le pagine \textsf{HTML}, per poterli ricercare agilmente.
}

\noindent Si passino ora in rassegna le operazioni fondamentali richieste per indicizzare una pagina:
\begin{enumerate}
	\item si dividono i \textit{tag} dal testo (che risulta una sequenza continua di caratteri) e, a seconda del criterio di indicizzazione che è stato scelto, si scarta ciò che non si reputa necessario;
	\item si suddivide il testo in \textit{token}. Si noti che, nelle lingue indoeuropee, questo processo è reso estremamente facile dalla presenza della spaziatura\footnote{Questo aspetto non vale per tutte le lingue in assoluto: per esempio, nella lingua birmana non ci sono spaziature e la divisione dei \textit{token} si evince esclusivamente dal contesto.} e dei segni d'interpunzione\footnote{La scelta di involvere anche la punteggiatura nella indicizzazione da un lato obbliga a riservare più spazio per l'indice, ma dall'altro permette di rispondere a query più accurate.};
	\item si effettua la normalizzazione dei \textit{token}, aggregando tutti i segmenti di testo che fanno riferimento alla stessa parola chiave (come, per esempio, ``Ciao'' e ``ciao''). Il criterio di normalizzazione dei termini dipende strettamente dalla quantità di risorse che si hanno a disposizione: infatti, se verso la fine degli anni '90 venivano applicate tecniche di normalizzazione molto stringenti per cercare di semplificare al massimo, quali ad esempio la conversione delle maiuscole in minuscole\footnote{In caso di parole polisemiche, come ``che'' (pronome) e ``Che'' (soprannome del noto Ernesto Guevara), una indicizzazione che contempla la distinzione tra lettere minuscole e maiuscole risulta particolarmente utile. In tal caso, si potrebbe anche valutare l'eventualità di restituire all'utente che cerca ``Che'' la combinazione di entrambe le alternative, anche se logicamente tale operazione richiede più tempo.} o l'eliminazione di tutti gli accenti, al giorno d'oggi si assiste all'applicazione di regole di normalizzazione più lasche.
	Una forma di normalizzazione molto radicale che è stata largamente utilizzata negli ultimi tempi è la troncatura (o \textit{stemming}), che consiste nell'indicizzare la radice linguistica di una parola piuttosto che la parola stessa: ad esempio, ``cane'' e ``cani'' vengono indicizzati con ``can''.
	La troncatura trova le sue origini nell'inglese, in cui il più delle volte è sufficiente togliere una ``s'' per passare dal plurale al singolare.
	Per le lingue romanze, come l'italiano, la troncatura non è così banale da applicare, in quanto spesso si rischia di tagliare quasi tutta la parola nelle declinazioni verbali, ottenendo delle radici molto piccole e creando inevitabilmente molta confusione.
\end{enumerate}

\vspace*{-.5\baselineskip}
\greybox{Osservazione}{%
\`E evidente che qualunque tecnica di normalizzazione che si possa immaginare può essere introdotta o in fase di indicizzazione, oppure in una fase successiva, ad esempio quando l'utente effettua una query: infatti, prendendo in considerazione la normalizzazione effettuata sugli accenti, nel primo caso due \textit{token}, come ``papà'' e ``papa'', restituiscono lo stesso risultato, mentre nel secondo caso, quando l'utente interroga l'indice, si effettua l'accesso ad una tabella di parole simili alla \textit{keyword} inserita nella query e si mescolano i risultati.
Il vantaggio del primo approccio consiste nel fatto di disporre di un indice di dimensioni più contenute e conseguentemente sarà necessario attendere meno tempo prima di ricevere il risultato.
Il vantaggio enorme del secondo approccio (più popolare al giorno d'oggi per via della diffusione di macchine più potenti e di algoritmi più performanti) consiste nel riuscire a gestire i \textit{token} con maggiore flessibilità: ad esempio, si potrebbe decidere di proiettare i risultati di ``papà'' e ``papa'' con diversa priorità, a seconda del contesto.
}

\noindent Una volta affrontate ad alto livello le tematiche più scottanti inerenti all'indicizzazione, si mappino tali concetti in un reale motore di ricerca.

%-------------------------------------------------------------------------------------%
\section{Problematiche}
\label{sec:Problematiche}
Si parta da una situazione di questo tipo, rappresentata in \autoref{fig:Situazione}: si è in possesso di una lista di $n$ documenti, che sono stati precedentemente raccolti nell'attività di crawling. Si immagini che tali documenti siano numerati da $0$ a $n-1$ e ad ognuno di questi sia associata una lista di \textit{token} che sono stati trovati al loro interno. In corrispondenza di ciascun \textit{token}, è associato un ulteriore numero\footnote{Tale valore assume un ruolo fondamentale, in quanto, a fronte del fatto che si può valutare di non indicizzare intere parole, possono esserci dei buchi all'interno della pagina: per esempio, se in un testo che incomincia con ``Romeo e Giulietta'' si decidesse di non voler indicizzare la congiunzione ``e'', si avrebbe ``Romeo'' in posizione $0$ e ``Giulietta'' in posizione $2$.}, che indica la sua posizione all'interno del documento.
In questa maniera, risulta estremamente facile ricavare le parole che compaiono in un certo documento. Tuttavia, l'obiettivo è ottenere il contrario: a partire da una lista di parole, occorre sapere in quali documenti compaiono.
Questo processo è denominato \textit{inversione}, in quanto idealmente è molto simile alla trasposizione di una matrice: presa in considerazione una matrice binaria avente per righe la lista di \textit{token} e per colonne la lista dei documenti, si procede inserendo in corrispondenza di ogni cella il valore $1$ se nel documento $i$ è presente il \textit{token} $j$, altrimenti il valore $0$, come riportato nella \autoref{fig:Situazione}.
Dal momento che si è in grado di scandire la matrice per colonne, che in altre parole significa che, dato un documento, si trovano i vari \textit{token} al suo interno, si effettua una trasposizione per poterla scandire per righe, secondo cui, a partire da un certo \textit{token}, si ricava in quali documenti è contenuto.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.36]{cap2/img/situazione}
	\caption{Esempio di una matrice binaria tra \textit{token} e documenti.}
	\label{fig:Situazione}
\end{figure}

\greybox{Osservazione}{%
Di fatto, il problema dell'\textit{inversione} può essere tradotto in un problema di ordinamento.
}

\noindent A questo punto, sorgono dunque due questioni principali:
\begin{itemize}
	\item come si possono ottenere i dati dai documenti in maniera scalabile?
	\item come si possono rappresentare in memoria i dati ottenuti?
\end{itemize}

\noindent Innanzitutto, si cerchi di risolvere la prima problematica prendendo in esame un esempio concreto, riportato in \autoref{fig:Lista-coppie}: dati tre generici documenti, si effettua una scansione del loro contenuto, assegnando un valore numerico progressivo a ciascun termine trovato.
Quindi, ciò che è stato ottenuto si può scrivere come lista di coppie $\langle \textsf{ID}\ \textit{documento}, \textsf{ID}\ \textit{token} \rangle$, da ordinare stabilmente in base alla componente di destra: in questo modo, è possibile trovare tutti i documenti in cui compare il \textit{token} $0$, poi tutti i documenti in cui compare il termine $1$, e via discorrendo.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.38]{cap2/img/lista-coppie}
	\caption{Costruzione della lista di \textit{token}, posti in ordine di apparizione.}
	\label{fig:Lista-coppie}
\end{figure}

\greybox{Osservazione}{%
Se si decidesse di tenere traccia anche della posizione di ogni \textit{token}, sarebbe necessario costruire una lista di triple $\langle \textsf{ID}\ \textit{documento}, \textsf{ID}\ \textit{token}, \textit{posizione} \rangle$. Una volta salvate tutte in memoria, si procede ordinando stabilmente per \textsf{ID} \textit{token}, memorizzando il risultato su disco.
}

\noindent Si tenga presente che, per il momento, si sta assumendo che tutti i coefficienti utilizzati per marcare gli oggetti introdotti, come per esempio gli $n$ valori associati ai documenti, siano numeri interi a 64 bit.
\`E chiaro che non si può minimamente ipotizzare di adottare una rappresentazione di questo tipo, in quanto l'indice risultante diventerebbe spropositato.
Occorre trovare una rappresentazione comoda, compressa e al tempo stesso leggibile molto rapidamente, per rispondere alle interrogazioni impiegando il minor tempo possibile, rimanendo tendenzialmente al di sotto dell'ordine di grandezza del millisecondo. Tale rappresentazione è riportata in corrispondenza del \autoref{ch:compressione}.

\greybox{Osservazione}{%
\`E impensabile che un indice possa risiedere interamente nella memoria di una sola macchina: occorre disporre di un metodo per costruire l'indice gradualmente, possibilmente sfruttando più macchine in parallelo, e poi fondere i risultati.
}

\noindent Facendo riferimento alla seconda domanda, per numerare i \textit{token} rinvenuti all'interno di ogni documento è possibile utilizzare una tabella di hash: nel momento in cui si trova un nuovo termine, lo si inserisce nella tabella, associando come valore dell'indice la dimensione della tabella stessa (che infatti ha acquisito un nuovo \textit{token}), altrimenti si incrementa il valore del termine opportuno.
Per ordinare stabilmente la lista di coppie ottenute rispetto a \textsf{ID} \textit{token}, dal momento che il numero di chiavi (ovvero \textsf{ID} \textit{documento}) è decisamente inferiore al numero di elementi da ordinare (ovvero \textsf{ID} \textit{token}), è molto efficace utilizzare un ordinamento per conteggio (o \textit{Counting Sort}), che richiede un tempo lineare.
Il problema di cui soffre questa strategia è che non è sempre applicabile: dal momento che la memoria occupata è proporzionale al numero di chiavi, di fronte ad un numero immenso di chiavi non si avrebbe dello spazio sufficiente per eseguire l'algoritmo.
Nella \autoref{subsec:countsort} è presentato il suo meccanismo di funzionamento nel dettaglio.

%-------------------------------------------------------------------------------------%
\subsection{Counting Sort}
\label{subsec:countsort}
Il \textit{Counting Sort} si basa su un'idea assolutamente elementare: in presenza di poche chiavi e di un numero di elementi ripetuti tante volte, si conta quante sono le istanze associate a ogni chiave, sfruttando banalmente una funzione cumulativa. Quindi, noto l'intervallo di valori gestito da ciascuna chiave, si inseriscono gli elementi direttamente al loro posto.
Per maggior chiarezza, si prenda in considerazione un esempio concreto, rappresentato in \autoref{fig:Countsort}, passando in rassegna le fasi dell'algoritmo:
\begin{enumerate}
	\item si crea un vettore C, avente una lunghezza pari al numero di chiavi presenti, con cui sia possibile contare le occorrenze di ogni chiave;
	\item a partire dal vettore C, si crea un vettore D, di analoga dimensione, contenente i relativi valori cumulativi;
	\item si crea un vettore di supporto O, avente una lunghezza pari alla dimensione della sequenza da ordinare I;
	\item si scandisce I e si inserisce in O l'elemento corrente nella posizione indicata dal valore corrispondente in D. Tale valore viene ogni volta incrementato di $1$.
\end{enumerate}

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.32]{cap2/img/countsort}
	\caption{Esempio di esecuzione dell'algoritmo \textit{Counting Sort}.}
	\label{fig:Countsort}
\end{figure}

\vspace*{-1.5\baselineskip}
\greybox{Osservazione}{%
Esiste una versione abbastanza complicata del \textit{Counting Sort}\footnotemark\ in cui si risparmia l'adozione del vettore di supporto O, ordinando gli elementi direttamente in I (in gergo, questo modo di operare è detto \textit{in-place}), ma che non è stabile.
}
\footnotetext{\fullcite{sedgewick2003algorithms}}

%-------------------------------------------------------------------------------------%
\section{Scrittura dell'indice}
\label{sec:indice}
Una volta che i dati ricavati dalla scansione dei documenti sono stati ordinati, è possibile scrivere su disco un file contenente l'indice desiderato, accessibile in varie modalità in funzione dell'architettura in adozione, come ad esempio mediante \textit{memory mapping}. Dal momento che tale file può assumere dimensioni considerevoli, si può pensare ad un sistema in cui tale operazione venga eseguita a blocchi, e quindi su più macchine, provvedendo in seguito a fondere le informazioni.
In tal caso, sorge però una questione: dal momento che diverse macchine potrebbero aver assegnato indici diversi ad una stessa parola, sarebbe necessario disporre di una sorta di meccanismo di sincronizzazione dei \textit{token}. Nello specifico, servirebbe una lista di termini che associ ad ogni \textit{token} un valore numerico, come riportato in \autoref{fig:indice}.
\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.35]{cap2/img/indice}
	\caption{Esempio di generazione dei file degli indici in parallelo.}
	\label{fig:indice}
\end{figure}
Tuttavia, questo non è sufficiente, in quanto, per effettuare una fusione degli indici, non è possibile applicare banalmente una concatenazione: bisogna infatti prendere le liste di documenti associati a ogni termine e concatenarle, ove necessario.
Ma applicare questa operazione a liste di termini non ordinate sarebbe impensabile, in quanto obbligherebbe a ricercare nell'indice la lista dei documenti associata ad ogni termine.
Quindi, optando per sistemare i termini in ordine lessicografico (o \textit{rango} lessicografico), anziché in ordine di apparizione (come svolto precedentemente in \autoref{fig:Lista-coppie}), è possibile scandire le liste in parallelo, leggendo e scrivendo gli indici in sequenza.
Per raggiungere tale scopo, occorre applicare un ordinamento indiretto dei \textit{token}, già affrontato nella \autoref{subsec:ord_ind}: una volta che è stata calcolata l'inversa della permutazione ordinante, si ottiene la posizione alfabetica di tutti i \textit{token} che sono stati rilevati.
Un esempio pratico dell'applicazione di questo meccanismo è stato riportato in corrispondenza di \autoref{fig:Ordinamento-token}.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.34]{cap2/img/ordinamento-token}
	\caption{Costruzione della lista di \textit{token}, posti in ordine alfabetico.}
	\label{fig:Ordinamento-token}
\end{figure}

\greybox{Osservazione}{%
L'attuazione dell'ordinamento lessicografico dei termini è una attività che si deve necessariamente verificare dopo aver riempito la memoria con tutte le parole. A quel punto, è possibile costruire il relativo indice e salvarlo su disco. Fino ad una sua eventuale prossima ricostruzione, l'indice rimane immutato, proprio per consentire dei tempi di risposta fulminei ad ogni interrogazione.
}

\noindent Dunque, la procedura appena descritta permette di realizzare un indice di \textit{token}, posti in ordine lessicografico, a cui è associata la lista di documenti in cui compaiono. Tale indice gode del vantaggio di poter essere fuso facilmente con altri indici.
In generale, in un indice è possibile trovare alcune situazioni un po' particolari, indicate con un nome specifico. Le più significative sono illustrate nella prossima sezione.

%-------------------------------------------------------------------------------------%
\section{Notazione}
\label{sec:notation}
\begin{itemize}
	\item \textbf{Hapax Legomena}: sono dei termini che compaiono in un solo documento. Per questo motivo, sono considerati scomodi da tenere memorizzati, in quanto ingombrano inutilmente la lista dei termini.
	\item \textbf{Stopword}: posti all'estremo opposto del caso precedente, sono dei termini che compaiono in tutti i documenti. \`E facile intuire che tali termini non si rivelano particolarmente utili per ricercare dei documenti interessanti.
\end{itemize}

\vspace*{-\baselineskip}
\greybox{Osservazione}{%
Nei primi algoritmi di costruzione di indici, tutti gli \textit{hapaxes} e tutte le \textit{stopwords} venivano appositamente eliminati per ridurne la dimensione.
}

\begin{itemize}
	\item \textbf{Affissione}: in previsione del grado di precisione che si vuole raggiungere nella fase di \textit{ranking}, è possibile valutare la possibilità di memorizzare diverse informazioni.
	Ad esempio, disporre di una lista composta esclusivamente da documenti permetterebbe di eseguire un \textit{ranking} di tipo booleano, secondo cui è possibile stabilire se un \textit{token} è presente o meno in un documento. In alternativa, si potrebbe valutare l'eventualità di salvare altre informazioni, come per esempio il numero di volte che un determinato \textit{token} compare in un documento e la lista delle posizioni in cui compare. In tal caso, si potrebbe dedurre la potenziale importanza di ogni documento, in quanto, ad una maggiore frequenza di apparizione di un certo \textit{token}, dovrebbe corrispondere una maggiore rilevanza del relativo documento. Tutte le informazioni che fanno riferimento ad un termine vengono chiamate \textit{affissioni}. Quindi, di fatto, un indice può essere visto come un insieme di liste di affissioni.
\end{itemize}

\vspace*{-\baselineskip}
\greybox{Osservazione}{%
Disponendo sia della lista dei \textit{token} che delle corrispettive affissioni complete, si potrebbe ricostruire i testi originali per intero.
}
\vspace*{-5px}

%-------------------------------------------------------------------------------------%
\section{Fusione di indici}
\label{sec:indexes-fusion}
Come è stato accennato nella \autoref{sec:indice}, la dimensione limitata della memoria RAM impone di salvare su disco più indici, ognuno dei quali fa riferimento ad un sottoinsieme preciso di documenti.
Dal momento che si vorrebbe attingere le informazioni raccolte da un unico indice, come se si avesse avuto a disposizione una macchina molto potente in grado di leggere tutti i documenti contemporaneamente, è necessario disporre di un meccanismo per fondere i vari indici.
Avendo l'accortezza di sistemare in ordine alfabetico la lista dei \textit{token} associati ad ogni indice, esiste una strategia, chiamata \textit{fusione multi-way}, con cui è possibile effettuare una fusione di tutti gli indici in una sola passata.

%-------------------------------------------------------------------------------------%
\subsection{Fusione multi-way}
\label{subsec:multi-way-fusion}
L'algoritmo di \textit{fusione multi-way} è la versione glorificata del banale algoritmo di fusione di due liste ordinate\footnote{Date due liste ordinate $L_1$ e $L_2$, si fissa sull'elemento minimo di entrambe un puntatore: eseguendo una scansione in parallelo, se l'elemento minimo si trova in una sola delle due liste, lo si estrae dalla relativa lista e si incrementa il puntatore, altrimenti, se si trova in entrambe, lo si estrae dalla entrambe le liste e si incrementa il puntatore.}, con cui è possibile fondere più liste utilizzando uno \textit{heap}\footnote{Uno \textit{heap} è una coda con priorità, in cui l'elemento posto in testa si ricava in tempo costante ed il valore di priorità associato all'elemento posto in testa si modifica in tempo logaritmico.} \textit{indiretto}\footnote{Le chiavi di ordinamento, ovvero i valori di priorità, sono esterni alla coda con priorità.}.
Per comprenderne il funzionamento più in dettaglio, si prenda in considerazione una situazione molto semplice, rappresentata in \autoref{fig:Fusione}, in cui siano dati tre file diversi, ognuno contenente una lista di \textit{token}. In corrispondenza del primo elemento di ogni lista, è presente un puntatore, che viene sfruttato per costruire la coda con priorità C: mantenendo un ordine alfabetico tra i \textit{token} puntati, si riporta il numero del file opportunamente associato.
Ad ogni passo dell'algoritmo:
\begin{enumerate}
	\item si accede al file, il cui riferimento numerico è posto in testa a C;
	\item si estrae l'elemento correntemente puntato in una lista risultante L;
	\item si aggiorna nel file la priorità, facendo avanzare il puntatore;
	\item si risistema la coda, tenendo conto del criterio di ordinamento.
\end{enumerate}

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.33]{cap2/img/fusione}
	\caption{Esempio di applicazione di \textit{fusione multi-way} a tre file.}
	\label{fig:Fusione}
\end{figure}

\greybox{Osservazione}{%
Dal momento che nella lista risultante L si memorizza il nome del file e l'elemento correntemente puntato, risulta sempre possibile ricostruire da quale file proviene ogni \textit{token}, che è fondamentale nel processo di fusione degli indici.
}

\noindent Il tempo di esecuzione di questa procedura è uguale alla somma di tutte le lunghezze delle liste di \textit{token}, poiché ogni istanza di ogni termine di ogni lista si trova almeno una volta in testa alla coda con priorità, moltiplicato per il logaritmo del numero delle liste, dal momento che occorre aggiornare la testa della coda con priorità.

%-------------------------------------------------------------------------------------%
\subsection{Fusione multi-way su indici}
\label{subsec:multi-way-indexes-fusion}
Applicando quanto è stato appena descritto ad un caso concreto di fusione di indici, illustrato per maggior chiarezza in \autoref{fig:Fusione_indici}, subentra una difficoltà aggiuntiva, rappresentata dal fatto di dover considerare, oltre ai file contenenti la lista di \textit{token}, anche le relative liste di affissioni.
Ciò che occorre fare è associare alla prima lista di affissioni di ogni indice un puntatore, che viene fatto avanzare in corrispondenza dell'aggiornamento del puntatore nella lista di \textit{token}.
Quindi, nel processo di costruzione dell'indice finale, quando viene rilevato che un \textit{token} è memorizzato su più file, è sufficiente concatenare le rispettive liste di affissioni.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.36]{cap2/img/fusione_indici}
	\caption{Esempio di applicazione di \textit{fusione multi-way} a tre indici.}
	\label{fig:Fusione_indici}
\end{figure}

\noindent Si noti che, in questa procedura, ogni indice viene attraversato solo una volta e in una sola direzione.
